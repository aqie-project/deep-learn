
import cv2
from modelscope.outputs import OutputKeys
from modelscope.pipelines import pipeline
from modelscope.utils.constant import Tasks

input_location = 'a.png'
input_mask_location = 'a1.png'
input = {
        'img':input_location,
        'mask':input_mask_location,
}

inpainting = pipeline(Tasks.image_inpainting, model='damo/cv_fft_inpainting_lama')
result = inpainting(input)
vis_img = result[OutputKeys.OUTPUT_IMG]
cv2.imwrite('result.png', vis_img)