```
pip install pipenv -i https://mirror.baidu.com/pypi/simple
pipenv install --python 3.11.1
pipenv shell
pipenv run pip list

# 图像填充

pip install opencv-python -i https://mirror.baidu.com/pypi/simple
pip install torch torchvision -i https://mirror.baidu.com/pypi/simple
pip install transformers -i https://mirror.baidu.com/pypi/simple
pip install sentencepiece -i https://mirror.baidu.com/pypi/simple
pip install kornia -i https://mirror.baidu.com/pypi/simple
pip install modelscope -i https://mirror.baidu.com/pypi/simple

pip install diffusers -i https://mirror.baidu.com/pypi/simple
pip install accelerate -i https://mirror.baidu.com/pypi/simple

pipenv install -r requirements.txt
pipenv requirements > requirements.txt
```